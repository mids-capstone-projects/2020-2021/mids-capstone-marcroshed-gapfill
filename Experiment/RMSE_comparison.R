rm(list = ls())
# Core Tidyverse
require(dplyr)
library(tidyverse)
library(glue)
library(forcats)
# Time Series
require(timetk)
require(tidyquant)
require(tibbletime)
library(imputeTS)
# Visualization
require(cowplot)
require(ggplot2)
# Preprocessing
require(recipes)
require(bayesbio)
# Sampling / Accuracy
require(rsample)
require(yardstick)
# Modeling
require(keras)
require(forecast)
require(caret)
require(vip)
library(e1071)
# feather data package
library(feather)

setwd("D:/Duke/2020_fall/capstone")
df <- read.csv('comparative-ecosystems/experiment/RMSE.csv')
df1 <- read.csv('comparative-ecosystems/experiment/RMSE1.csv')
colnames(df)[1] <-"solute"
colnames(df1)[1]<-"solute"
df[,1] <- df[,1] %>% str_replace(pattern="GN_", replacement = "")
df1[,1] <- df1[,1] %>% str_replace(pattern="GN_", replacement = "")

plot1 <- ggplot() +
  geom_point(data = df, 
             aes(x = solute, y = LSTM_rmse, color = "LSTM"),alpha = 0.5 ) +
  geom_point(data = df, 
             aes(x = solute, y = RF_rmse, color = "RF"),alpha = 0.5 ) +
  geom_point(data = df, 
             aes(x = solute, y = SVM_rmse, color = "SVM"),alpha = 0.5 ) +
  geom_jitter(height = 0.8, width = 0.5) + theme_test()+ 
  labs( x = 'Solute',
        y = "RMSE of test dataset")+
  scale_colour_manual(values=c("black","blue","red"))

print(plot1)


plot2 <- ggplot(df1, aes(solute, RMSE)) +
  geom_jitter(height = 0.2, width = 0,aes(colour = method)) + theme_test()+ 
  labs( x = 'Solute',
        y = "RMSE of test dataset")#+
  #scale_colour_manual(values=c("black","blue","red"))
print(plot2)
