#rm(list = ls()) #clear wd

## this is a sample data wrapper function 

list.of.packages <- c("dplyr", "tidyverse", "glue", "forcats", "tidyquant", "tibbletime", 
                      "imputeTS", "cowplot", "ggplot2", "recipes", "bayesbio")

new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages)

rm(list.of.packages)
rm(new.packages)

# Core Tidyverse
library(dplyr)
library(tidyverse)
library(glue)
library(forcats)

# Time Series
#library("timetk")
library(tidyquant)
library(tibbletime)
library(imputeTS)

# Visualization
library(cowplot)
library(ggplot2)

# Preprocessing
library(recipes)
library(bayesbio)

# # Sampling / Accuracy
# library(rsample)
# library(yardstick)
#
# # Modeling
# library(keras)
# library(forecast)
# library(caret)
# library(vip)
# library(e1071)

# feather data package
library(feather)
library(zoo)

#setwd("D:/Duke/2020_fall/capstone")

#for testing data out

# grab <- read_feather('./data/w3_grab.feather')
# sensor <- read_feather('./data/w3_sensor_wdisch.feather')
# grab.sort <- grab[order(grab$datetime), ]
# sensor.sort <- sensor[order(sensor$round_datetime), ]

#for debugging code: 
# 
# sensor = sensor.sort
# grab = grab.sort
# lags=0 #optional
# train_ratio = 0.8#optional
# DOY = TRUE#optional
# DOW = TRUE#optional
# WOY = TRUE
# MOY = TRUE
# flow_stats = TRUE

preprocess <- function(sensor, #req para
                        grab,   #req para
                        lags=0, #optional
                        train_ratio = 0.8,#optional
                        DOY = TRUE,#optional
                        DOW = TRUE,#optional
                        WOY = TRUE,
                        MOY = TRUE, 
                        flow_stats = TRUE #default
){
  
  message("Starting data pre-processing...")
 
  sensor <- sensor[!sensor$date %in% seq.Date(max(grab$date), max(sensor$date), by = "1 day"),] #add date check
  
  ncol<-ncol(sensor)
  colname <- colnames(sensor)[-which(colnames(sensor) %in% c("date","datetime", "round_datetime") )]
  names <- colnames(sensor[,-which(names(sensor) %in% c("date","datetime", "round_datetime"))])
  #impute sensor data using imputeTS
  for (i in 1:ncol){
    sensor[i] <- imputeTS::na_seadec(sensor[i], find_frequency = TRUE) #this doesn't actually fill NA's for big gaps; only small gaps 
    #fill na using TS fillna function
  }
  #initial newset for output
  newset <- sensor[,-which(names(sensor) %in% c("date","datetime"))]
 
  message("Impute timeseries complete [1/5]")
  
  # add columns for lags to sensor dataset
  nrow <- nrow(sensor)
  if (lags>0){
    nrow <- nrow(sensor)
    sensor.1 <- sensor[,-which(names(sensor) %in% c("date","datetime", "round_datetime"))]
    newset <- sensor[(lags+1):nrow,-which(names(sensor) %in% c("date","datetime"))]#re-initialize newset
    for (i in 1:lags){
      names(sensor.1) <- paste0(colname,"_lag", i)#modify sensor.1's column names with lags
      newset <- cbind(newset, sensor.1[(lags - i+1):(nrow-i),])#attach lags into columns to output data
    }
  }
  
  message("Added lags [2/5]")
  
  #now, sensor data has lags in the columns
  if (DOY == TRUE){
    # add DOY info
    newset <- newset %>%
      mutate(DOY = lubridate::yday(round_datetime))
  }
  
  if (DOW == TRUE){
    # add DOY info
    newset <- newset %>%
      mutate(DOW = lubridate::wday(round_datetime))
    
    newset$DOW <- factor(newset$DOW, exclude = NULL)
  }
  
  if (WOY == TRUE){
    # add DOY info
    newset <- newset %>%
      mutate(WOY = lubridate::week(round_datetime))
    
    newset$WOY <- factor(newset$WOY, exclude = NULL)
  }
  
  if (MOY == TRUE){
    # add DOY info
    newset <- newset %>%
      mutate(MOY = lubridate::month(round_datetime))
    newset$MOY <- factor(newset$MOY, exclude = NULL)
  }
  
  message("Added time variables [3/5]")
  
  if (flow_stats == TRUE) {
    
    #get column with discharge in it 
    Q_colname <- grep("disch", colnames(sensor), value = T)
    
    #add log_Q to newset (gap-filled sensor data)
    newset["log_Q"] <- log(newset[Q_colname])
    #newset <- newset %>% mutate(date = date(round_datetime))
    
    #have new complete dataframe 
    complete <- left_join(newset, grab, by = c("round_datetime" = "datetime")) %>% 
      select(-date) %>% 
      mutate(date = date(round_datetime))
    
    #get just the Q from this 
    Q_only <- newset %>% select(round_datetime, as.name(Q_colname))
    Q_stats <- Q_only %>% 
      dplyr::group_by(date = date(round_datetime)) %>% 
      summarise(max_IS_discharge = max(IS_discharge, na.rm = T),
                mean_IS_discharge = mean(IS_discharge, na.rm = T),
                min_IS_discharge = min(IS_discharge, na.rm = T), 
                sd_IS_discharge = sd(IS_discharge, na.rm = T))
    
    
    get_rolling <- function(rowdate, n_days, calc){
      
      #rowdate <- date(Q_stats[105,]$date)
      priordate <- rowdate - n_days
      rollingdates <- seq.Date(from = priordate, rowdate, by = "1 day")
      
      if(calc == "mean"){val <- 
        mean(Q_stats[which(Q_stats$date %in% rollingdates),]$mean_IS_discharge, na.rm = T)} 
      if(calc == "max") {val <-
        max(Q_stats[which(Q_stats$date %in% rollingdates),]$mean_IS_discharge, na.rm = T)} 
      if(calc == "min") {val <- 
        min(Q_stats[which(Q_stats$date %in% rollingdates),]$mean_IS_discharge, na.rm = T)}
      
      return(val)
      
    } 
    
    Q_stats$Q_7_mean<- mapply(get_rolling, Q_stats$date, 7, "mean")
    Q_stats$Q_7_max <- mapply(get_rolling, Q_stats$date, 7, "max")
    Q_stats$Q_7_min <- mapply(get_rolling, Q_stats$date, 7, "min")
    Q_stats$Q_7_mean<- mapply(get_rolling, Q_stats$date, 7, "mean")
    Q_stats$Q_7_max <- mapply(get_rolling, Q_stats$date, 7, "max")
    Q_stats$Q_7_min <- mapply(get_rolling, Q_stats$date, 7, "min")
    
    Q_stats$Q_30_mean<- mapply(get_rolling, Q_stats$date, 30, "mean")
    Q_stats$Q_30_max <- mapply(get_rolling, Q_stats$date, 30, "max")
    Q_stats$Q_30_min <- mapply(get_rolling, Q_stats$date, 30, "min")
    Q_stats$Q_30_mean<- mapply(get_rolling, Q_stats$date, 30, "mean")
    Q_stats$Q_30_max <- mapply(get_rolling, Q_stats$date, 30, "max")
    Q_stats$Q_30_min <- mapply(get_rolling, Q_stats$date, 30, "min")
    
    complete <- left_join(complete, Q_stats, by = c("date"))
    newset <- left_join(newset %>% mutate(date = date(round_datetime)), Q_stats, by = "date")
    
    complete <- complete %>% select(!date) #remove date from dataframe; needed this for discharge merge
    newset <- newset %>% select(!date)
    
  } else {warning("no flow statistics calculated from discharge data")
    #merge sensor (predictor) and grab (targe) data
    complete <- merge(newset, grab, by.x="round_datetime", by.y = "datetime", all.x =T) 
    # use left join to avoid create new null rows
    }
  # since full join will create NA in sensor data columns, need to impute again
  #for (i in 1:(ncol*(1+lags))){
  #  complete[i]<- imputeTS::na_seadec(complete[i], find_frequency = TRUE)
  #
  #}
  
  ## add check on data that there are no dates beyond sensor 
  
  
  
  message("Added hydrological variables [4/5]")
  
  nrow <- nrow(complete)
  result <- list(sensor_lags = newset,
                 train_val_data = complete[1:((nrow*10*train_ratio)%/%10),],
                 test_data= complete[(((nrow*10*train_ratio)%/%10)+1):nrow,],
                 x_colnams = colnames(newset[,-which(names(newset) %in% c("round_datetime"))]),
                 y_colnams = colnames(grab)[-which(colnames(grab) %in% c("date","datetime", "round_datetime", "site_name"))])
  
  
  message("Completed outputs [5/5]")
  
  result
}

# return list of datasets (sensor (no merge), train (merged), test(merged))
#preprocessed_sets <- preprocess(sensor.sort, grab.sort, lags = 3, flow_stats = T) #this takes about 30 seconds to run

#td <- preprocessed_sets[[2]]


##############
## SANDBOX ## 
#############

# this wasn't inlucded 

# td_before <-  sensor.sort %>% select(date, round_datetime, IS_discharge)
# 
# get_rolling_slope <- function(time, hours) {
# 
#   #hours <- 6
#   #time <- td_before$round_datetime[10]
# 
#   endtime <- time + (hours*60*60)
#   seq_time <- seq.POSIXt(from = time, to = endtime, by = "15 mins")
#   subset <- td_before %>% filter(round_datetime %in% seq_time)
# 
#   first_val <- subset$IS_discharge[1]
#   last_val <- subset$IS_discharge[nrow(subset)]
# 
#   slope <- (last_val - first_val)/nrow(subset)
# 
#   return(slope)
# }
# 
# td_before$log_Q <- log(td_before$IS_discharge)
# 
# 
# td_before$slope_3hr <- mapply(get_rolling_slope, td_before$round_datetime[1:100], 3)
# #td_before$slope_5hr <- mapply(get_rolling_slope, td_before$round_datetime, 5)
# td_before$slope_1hr <- mapply(get_rolling_slope, td_before$round_datetime, 1)


