## making figures for model testing and review 

library(readxl)
library(tidyverse)
library(data.table)
library(ggpubr)

colorblind_blue <- "#0C7BDC"
colorblind_yellow <- "#FFC20A"

testing_error_summary <- read_excel("codes/model_testing/testing_error_summary.xlsx")
testing_error_summary_pt2 <- read_excel("codes/model_testing/testing_error_summary.xlsx", sheet = 3)

testing_error_summary$model <- ifelse(grepl("rf", testing_error_summary$name), "RF", "SVM")

rf_svm_rmse <- 
  ggplot(data = testing_error_summary) + 
  geom_line(aes(x = lags, y = rmse_train, color = flowstats), linetype = "dashed") +
  geom_line(aes(x = lags, y = rmse_test, color = flowstats)) + 
  geom_point(aes(x = lags, y = rmse_train, color = flowstats, shape = flowstats)) +
  geom_point(aes(x = lags, y = rmse_test, color = flowstats, shape = flowstats)) +
  facet_wrap(~model) + scale_y_continuous(breaks = seq(0,.2, 0.025)) +
  theme_test(base_size = 15) +
  scale_color_manual(values = c(colorblind_blue, colorblind_yellow))

rf_svm_mae <- 
  ggplot(data = testing_error_summary) + 
  geom_line(aes(x = lags, y = MAE_train, color = flowstats), linetype = "dashed") +
  geom_line(aes(x = lags, y = MAE_test, color = flowstats)) + 
  geom_point(aes(x = lags, y =MAE_train, color = flowstats, shape = flowstats)) +
  geom_point(aes(x = lags, y =MAE_test, color = flowstats, shape = flowstats)) +
  facet_wrap(~model) + scale_y_continuous(breaks = seq(0,.2, 0.025)) +
  theme_test(base_size = 15) + 
  scale_color_manual(values = c(colorblind_blue, colorblind_yellow))

rf_svm_accuracy <- 
  ggplot(data = testing_error_summary) + 
  geom_line(aes(x = lags, y = 1-MAE_train, color = flowstats), linetype = "dashed") +
  geom_line(aes(x = lags, y = 1-MAE_test, color = flowstats)) + 
  geom_point(aes(x = lags, y =1-MAE_train, color = flowstats)) +
  geom_point(aes(x = lags, y =1-MAE_test, color = flowstats)) +
  facet_wrap(~model) + 
  scale_y_continuous(breaks = seq(0,1, 0.2), limits = c(0,1)) +
  theme_test()

########### now for size of dataset 

lstm_rmse <- 
  ggplot(data = testing_error_summary_pt2 %>% filter(model == "lstm")) + 
  geom_line(aes(x = ncol, y = rmse_train, color = flowstats), linetype = "dashed") +
  geom_line(aes(x = ncol, y = rmse_test, color = flowstats)) + 
  geom_point(aes(x = ncol, y = rmse_train, color = flowstats, shape = flowstats)) +
  geom_point(aes(x = ncol, y = rmse_test, color = flowstats, shape = flowstats)) +
  facet_grid(~samplingregime) + scale_y_continuous(breaks = seq(0,.8, 0.05)) +
  theme_test(base_size = 15) +
  scale_color_manual(values = c(colorblind_blue, colorblind_yellow))

rf_rmse <- 
  ggplot(data = testing_error_summary_pt2 %>% filter(model == "rf")) + 
  geom_line(aes(x = ncol, y = rmse_train, color = flowstats), linetype = "dashed") +
  geom_line(aes(x = ncol, y = rmse_test, color = flowstats)) + 
  geom_point(aes(x = ncol, y = rmse_train, color = flowstats, shape = flowstats)) +
  geom_point(aes(x = ncol, y = rmse_test, color = flowstats, shape = flowstats)) +
  facet_grid(~samplingregime) + scale_y_continuous(breaks = seq(0,.8, 0.05)) +
  theme_test(base_size = 15) +
  scale_color_manual(values = c(colorblind_blue, colorblind_yellow))

svm_rmse <- 
  ggplot(data = testing_error_summary_pt2 %>% filter(model == "svm")) + 
  geom_line(aes(x = ncol, y = rmse_train, color = flowstats), linetype = "dashed") +
  geom_line(aes(x = ncol, y = rmse_test, color = flowstats)) + 
  geom_point(aes(x = ncol, y = rmse_train, color = flowstats, shape = flowstats)) +
  geom_point(aes(x = ncol, y = rmse_test, color = flowstats, shape = flowstats)) +
  facet_grid(~samplingregime) + scale_y_continuous(breaks = seq(0,.8, 0.05)) +
  theme_test(base_size = 15) +
  scale_color_manual(values = c(colorblind_blue, colorblind_yellow))

lstm_MAE <- 
  ggplot(data = testing_error_summary_pt2 %>% filter(model == "lstm")) + 
  geom_line(aes(x = ncol, y = MAE_train, color = flowstats), linetype = "dashed") +
  geom_line(aes(x = ncol, y = MAE_test, color = flowstats)) + 
  geom_point(aes(x = ncol, y = MAE_train, color = flowstats, shape = flowstats)) +
  geom_point(aes(x = ncol, y = MAE_test, color = flowstats, shape = flowstats)) +
  facet_grid(~samplingregime) + scale_y_continuous(breaks = seq(0,.8, 0.05)) +
  theme_test(base_size = 15) +
  scale_color_manual(values = c(colorblind_blue, colorblind_yellow))

rf_MAE <- 
  ggplot(data = testing_error_summary_pt2 %>% filter(model == "rf")) + 
  geom_line(aes(x = ncol, y = MAE_train, color = flowstats), linetype = "dashed") +
  geom_line(aes(x = ncol, y = MAE_test, color = flowstats)) + 
  geom_point(aes(x = ncol, y = MAE_train, color = flowstats, shape = flowstats)) +
  geom_point(aes(x = ncol, y = MAE_test, color = flowstats, shape = flowstats)) +
  facet_grid(~samplingregime) + scale_y_continuous(breaks = seq(0,.8, 0.05)) +
  theme_test(base_size = 15) +
  scale_color_manual(values = c(colorblind_blue, colorblind_yellow))

svm_MAE <- 
  ggplot(data = testing_error_summary_pt2 %>% filter(model == "svm")) + 
  geom_line(aes(x = ncol, y = MAE_train, color = flowstats), linetype = "dashed") +
  geom_line(aes(x = ncol, y = MAE_test, color = flowstats)) + 
  geom_point(aes(x = ncol, y = MAE_train, color = flowstats, shape = flowstats)) +
  geom_point(aes(x = ncol, y = MAE_test, color = flowstats, shape = flowstats)) +
  facet_grid(~samplingregime) + scale_y_continuous(breaks = seq(0,.8, 0.05)) +
  theme_test(base_size = 15) +
  scale_color_manual(values = c(colorblind_blue, colorblind_yellow))

pdf(width = 6, height = 4, "plots_for_testing.pdf")
rf_svm_rmse
rf_svm_mae
lstm_rmse
rf_rmse
svm_rmse
lstm_MAE
rf_MAE
svm_MAE
dev.off()