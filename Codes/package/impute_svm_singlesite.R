############################################
## SVM - model 1
## Roderick Whang
############################################

list.of.packages <- c("dplyr", "tidyverse", "glue", "forcats", "timetk", "tidyquant", "tibbletime", "imputeTS",
                      "cowplot", "ggplot2", "recipes", "bayesbio", "rsample", "yardstick", "keras", "forecast",
                      "caret", "vip", "e1071", "feather", "rminer")

new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages)

rm(list.of.packages)
rm(new.packages)

#setwd("C:/Capstone")
#grab <- read_feather('./w3_grab.feather')
#sensor <- read_feather('./w3_sensor_wdisch.feather')
#grab.sort <- grab[order(grab$datetime), ]
#grab[grab.sort$datetime != grab$datetime,]
#sensor.sort <- sensor[order(sensor$round_datetime), ]
#sensor[sensor.sort$datetime != sensor$datetime,]


######### preprocessing function #########
#source('./codes/preprocessing_function.R')
#source('./preprocessing_function.R')

#preprocessed_sets <- preprocess(sensor.sort,grab.sort,lags =3)
# preprocessed_sets = processed.dataset
# solute = target.solute

# changed this from target to solute on 24-March-2021

impute_svm_singlesite <- function(preprocessed_sets, solute) {
  #data preparation
  message("model fitting")
  
  traindata <- preprocessed_sets$train_val_data[,which(names(preprocessed_sets$train_val_data) %in% c("round_datetime", append(preprocessed_sets$x_colnams, solute)))]
  traindata <- drop_na(traindata)
  ntrain <- nrow(traindata)

  train.x <- traindata[,which(names(traindata) %in% preprocessed_sets$x_colnams)]
  train.y <- traindata[,which(names(traindata) %in% solute)]

  testdata <- preprocessed_sets$test_data[,which(names(preprocessed_sets$test_data) %in% c("round_datetime", append(preprocessed_sets$x_colnams, solute)))]
  testdata <- drop_na(testdata)

  test.x <- testdata[,which(names(testdata) %in% preprocessed_sets$x_colnams)]
  test.y <- testdata[,which(names(testdata) %in% solute)]

  result <- list(train = list(x= train.x, y = train.y),
                 test = list(x = test.x, y = test.y))

  fm_vars <- paste(preprocessed_sets[["x_colnams"]], collapse = "+ ")
  fm <- as.formula(paste(solute, "~", fm_vars,
                         collapse = " ")) #make forumla with all the predictors

  # fitting
  tuneResult <- tune(svm, fm, data=traindata,type='eps-regression', kernel = 'radial',
                     ranges = list(epsilon = seq(0,0.2,0.01), cost = 2^(2:9)))

  model <- tuneResult$best.model
  message("predicting and model evaluation")
  # Predictions
  predictions_train <- predict(model, train.x) # predict training data
  predictions_test <- predict(model, test.x) # predict test data


  # error calculations
  error_summary <-
    data.frame(solute = solute,
               params = tuneResult$best.parameters,
               rmse_train = RMSE(predictions_train, train.y, na.rm = T),
               MAE_train = MAE(predictions_train, train.y, na.rm = T),
               rmse_test = RMSE(predictions_test, test.y, na.rm = T),
               MAE_test = MAE(predictions_test, test.y, na.rm = T))
  #print(error_summary)

  #fileName <- paste0(target, "_error_summary.csv") # write file
  #write.csv(file=fileName, error_summary, row.names = F)

  color_blue <- "#0072B2"

  #png(paste0(target,"_train_plot.png"))
  message("VI calculation and output plots")

  train_plot <- ggplot(data = traindata) +
    geom_point(aes(x = round_datetime , y = train.y), color = "black") +
    geom_line(aes(x = round_datetime , y = train.y), color = "black") +
    geom_point(aes(x = round_datetime , y = predictions_train), color = color_blue) +
    geom_line(aes(x = round_datetime , y = predictions_train), color = color_blue) +
    theme_test(base_size = 15) +
    labs(title = solute, subtitle = "Training data")+
    ylab(paste(solute, "mg/L")) + xlab("Date")
  train_plot

  #print(train_plot)
  #dev.off()

  #png(paste0(target,"_test_plot.png"))

  test_plot <- ggplot(data = testdata) +
    geom_point(aes(x = round_datetime , y = test.y), color = "black") +
    geom_line(aes(x = round_datetime , y = test.y), color = "black") +
    geom_point(aes(x = round_datetime , y = predictions_test), color = color_blue) +
    geom_line(aes(x = round_datetime , y = predictions_test), color = color_blue) +
    theme_test(base_size = 15)+
    labs(title = solute, subtitle = "Test data")+
    ylab(paste(solute, "mg/L")) + xlab("Date")
  test_plot

  #print(test_plot)
  #dev.off()

  # Variable Importance
  df<-as.data.frame(traindata)
  M <- fit(fm, data=df, model="svm", kpar=list(sigma=0.10), C=2)
  svm_imp <- Importance(M, data=df)
  results <- data.frame(feature=colnames(df),rate=svm_imp$imp)
  varimp <- results[order(results$rate, decreasing = TRUE),]
  #fileName <- paste0(target, "_variable_importance.csv") # write file
  #write.csv(file=fileName, varimp, row.names = F)


  varimp_top <- varimp[1:20, ]
  #print(varimp_top)

  #png(paste0(target,"_var_imp.png"))
  var_imp <- ggplot( varimp_top, aes( x = feature, y = rate ) ) +
    geom_bar( stat = "identity", fill="forest green" ) + ggtitle("Top 20 Variable Importance") +
    xlab("features") + ylab(paste(solute, "Variable Importance (rate)"))
  var_imp_plot <- var_imp + coord_flip() # bar gragh

  #print(var_imp)
  #dev.off()

  ## the following are to make the sensor plot with predicted values
  sensor_filter_var <- preprocessed_sets[["x_colnams"]][1]

  train_data <- preprocessed_sets[["train_val_data"]] %>%
    filter(complete.cases(!!as.name(solute))) %>%
    filter(complete.cases(!!as.name(sensor_filter_var))) 

  #testing data
  test_data <- preprocessed_sets[["test_data"]] %>%
    filter(complete.cases(!!as.name(solute))) %>%
    filter(complete.cases(!!as.name(sensor_filter_var))) 

  sensor_full <- preprocessed_sets[["sensor_lags"]] 
  predictions_mod <- model %>% predict(sensor_full)

  solute_full_df <-
    suppressMessages(full_join(test_data, train_data) %>% select(round_datetime, !!as.name(solute)))

  predicted_15min <- xts(x = predictions_mod, order.by = sensor_full$round_datetime)
  solute_xts <- xts(x = solute_full_df, order.by = solute_full_df$round_datetime)
  all_xts <- cbind(predicted_15min, solute_xts)

  predicted_plot <-
    dygraph(all_xts, main = paste0("Gap-fill predictions for ", solute)) %>%
    dyRangeSelector() %>%
    dyAxis("y", label = solute) %>%
    dyOptions(axisLineWidth = 1.5, drawGrid = FALSE, pointSize = 2.5) %>%
    dySeries(solute, drawPoints = TRUE, color = "black") %>%
    dySeries("predicted_15min", color = color_blue)

  complete_dataset_withmodel <-
    suppressMessages(left_join(data.frame(sensor_full, predictions_mod), solute_full_df))
  colnames(complete_dataset_withmodel)[which(
    colnames(complete_dataset_withmodel)=="predictions_mod")] <- paste0("PR_",solute)

  return_list <-
    list(model,
         complete_dataset_withmodel,
         error_summary,
         varimp_top,
         var_imp_plot,
         train_plot,
         test_plot,
         predicted_plot)

  names(return_list) <- c("model",
                          "model_output_df",
                          "model_error_summary",
                          "var_imp_top",
                          "var_imp_plot",
                          "train_plot",
                          "test_plot",
                          "predicted_plot")
  #names(return_list) <- c("model", "error_summary" )

  # save model
  #savelocation <- "./models/"
  #filename <- paste0(target, "_SVMmodel", ".rds")
  #saveRDS(model, paste0(savelocation, filename))
  #print(model)
  return(return_list)

}


