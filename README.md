**Manuscript information**

Understanding and monitoring water quality is important for researchers, land managers, and the general public alike. Most monitoring locations collect two kinds of water quality data: from grab samples at some uniform interval, and from water quality sensors, which take measurements at a frequent, often 15-minute interval. However these sampling regimes vary by sampling frequency and often contain data gaps or other quality control issues. In addition, we currently can detect a limited number of solutes at this higher sampling interval. Finally, grab sample regimes are often time intensive and costly, limiting the sampling frequency at many monitoring stations. We hope to leverage the underlying relationships between different sensed and non-sensed solutes using machine learning time series modelling techniques to impute grab sample solutes during times when they are not measured. We used several different machine learning models, LSTM, SVM, and RF, to impute weekly solute data every 15 minutes using existing sensor data at Hubbard Brook Experimental Forest (HBEF). No one modelling type outperformed the other on all fronts. In general, LSTM performed better at predicting data architecture (e.g. peaks in the data) and among all soutes, but did not do as well when the data contained gaps, or did not have many observations. By contrast, RF performed better when the data were gappy, over a longer period of observation, while SVM was better for shorter periods of observation. Because we only tested our approach at HBEF, we suggest that more monitoring locations in diverse hydrological and geographic settings utilize our model to evaluate its generalizability, as each result was highly dependent both on the target estimated grab sample solute and on the completeness of the data. 

Paper (upload to gitlab and create link) 
You can watch our final presentation at this link: [Final Comparative Ecosystems Team Presentation](https://drive.google.com/file/d/1I_yE_-WjmkLDgODmwUCo3HlQS872_hNM/view?usp=sharing) (April 2021) 

**Code Documentation**

Explanation of functions 
- preprocess()
    this function take raw macrosheds data and cleans it using impute_ts to fill gaps < 1 day wide, and adds other features like seasonal   indices (e.g. day of year - DOY) or watershed statistics from the EGRET R package, like minimum daily discharge   
- Impute_rf_singlesite()
    this function uses data from the preprocessed function to run a random forest model 
- impute_svm_singlesite()
- impute_lstm.fit_singlesite()
- impute_lstm.fill_singlesite()


To watch a video demo of our create functions, see this link: [Comparative Ecosystems package funcion demo](https://drive.google.com/file/d/1XnFNB25wrYqWYVAoN72vCiXC9QcT_P6b/view?usp=sharing) (March 2021) 

**Folder Heirarchy** 

- Documents (Sang-Jyh)
    - Final Paper 
    - Final presenation slides  
- Codes 
    - Model_testing codes   
    - Package (contains impute_**_singlesite packages)
    - Preprocessing function (this is the final preprocessing function)
- Data 
    - three .feather files with grab sample data, sensor data, and sensor data with discharge (all publically acessible, see paper)
    - these three files are already in-line with MacroSheds formatting
- Experiment (this folder is for other applications of our model, see supplement in final paper)
    - LSTM (extra jupyter notebooks for trial runs of models) 

